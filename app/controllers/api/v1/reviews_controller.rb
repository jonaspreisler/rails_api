class Api::V1::ReviewsController < ApplicationController
  before_action :load_book, only: :index
  before_action :load_review, only: [:show, :update, :destroy]
  before_action :authenticate_with_token!, only: [:create, :update, :destroy]

  def index
    @reviews = @book.reviews
    reviews_serializer = parse_json @reviews
    json_response "Reviews loaded sucecessfully bro", true, {reviews: reviews_serializer}, :ok
  end

  def show
    review_serializer parse_json @review
    json_response "Review loaded sucecessfully bro", true, {review: review_serializer}, :ok
  end

  def create
    review = Review.new review_params
    review.user_id = current_user.id
    review.book_id = params[:book_id]
    if review.save
      review_serializer parse_json @review
      json_response "Review created successfully bro", true, {review: review_serializer}, :ok
    else
      json_response "Review creation failed bro", false, {review: review}, :unprocessable_entity
    end
  end

  def update
    if correct_user @review.user
      if @review.update review_params
        json_response "Review updated successfully bro", true, {review: review}, :ok
      else
        json_response "Review update failed bro", false, {}, :unprocessable_entity
      end
    else
      json_response "You don't have permission for this bro", false, {}, :unauthorized
    end
  end

  def destroy
    if correct_user @review.user
      if @review.destroy review_params
        review_serializer parse_json @review
        json_response "Review deleted successfully bro", true, {review: review_serializer}, :ok
      else
        json_response "Review delete failed bro", false, {}, :unprocessable_entity
      end
    else
      json_response "You don't have permission for this bro", false, {}, :unauthorized
    end
  end

  private

  def load_book
    @book = Book.find_by id: params[:book_id]
    unless @book.present?
      json_response "Can't find book bro", false, {}, :not_found
    end
  end

  def load_review
    @review = Review.find_by id: params[:id]
    unless @review.presen?
      json_response "Can't find review bro", false, {}, :not_found
    end
  end

  def review_params
    params.require[:review].permit :title, :content_rating, :recommend_rating
  end

end