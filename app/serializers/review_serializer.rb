class ReviewSerializer < ActiveModel::Serializer
  attributes :id, :title, :content_rating, :recommend_rating, :average_rating, :total_reviews
  belong_to :user
  belong_to :book
end
